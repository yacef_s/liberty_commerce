<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});*/



Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/admin', [\App\Http\Controllers\admin::class, 'admin'])->name('admin');
//Route::get('/admin', [\App\Http\Controllers\admin::class, 'ajout'])->name('ajout');
Route::get('/accueil', [App\Http\Controllers\accueil::class, 'accueil'])->name('accueil');
Route::get('/catalogue', [App\Http\Controllers\catalogue::class, 'catalogue'])->name('catalogue');
Route::post('/catalogue', [App\Http\Controllers\catalogue::class, 'catalogue'])->name('catalogue');
Route::get('/produit/{id}', [App\Http\Controllers\produit::class, 'produit'])->name('produit');
Route::get('/inscription', [App\Http\Controllers\inscription::class, 'inscription'])->name('inscription');
Route::post('/inscription', [App\Http\Controllers\inscription::class, 'formulaire'])->name('inscription');
Route::get('/connexion', [App\Http\Controllers\connexion::class, 'connexion'])->name('connexion');
Route::post('/connexion', [App\Http\Controllers\connexion::class, 'traitement'])->name('traitement');


Route::get('/', [App\Http\Controllers\connexion::class, 'connexion'])->name('connexion');
Route::post('/', [App\Http\Controllers\connexion::class, 'traitement'])->name('traitement');
//Route::view('/catalogue', 'catalogue');

//Route::get('/admin', [App\Http\Controllers\admin::class, 'admin'])->name('admin');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
