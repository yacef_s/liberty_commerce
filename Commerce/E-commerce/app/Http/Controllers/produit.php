<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class produit extends Controller
{
    //
    public function produit(Request $request) {

        //dd($request->id);
        $produit = \App\Models\Catalogue::find($request->id);

        return view('produit', compact('produit'));

    }
}
