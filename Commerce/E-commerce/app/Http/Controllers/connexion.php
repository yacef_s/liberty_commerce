<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class connexion extends Controller
{
    //
    public function connexion() {

        return view('connexion');
    }

    public function traitement() {

        \request()->validate([
           'email' => ['required'],
           'psw' => ['required'],
        ]);

        $resultat = auth()->attempt([
            'email' => \request('email'),
            'mot_de_passe' => \request('psw'),
        ]);

        if ($resultat) {

            return redirect('/catalogue');

        }



        return back()->withInput();
    }
}
