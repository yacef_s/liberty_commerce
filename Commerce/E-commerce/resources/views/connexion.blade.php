@extends('layouts/layout')

@section('contenu')
    <link href="{{ asset('css/connexion.css') }}" rel="stylesheet">

    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <form action="/catalogue" method="post">
        {{csrf_field()}}
        <div class="imgcontainer">
        </div>

        <div class="container">
            <label for="email"><b>email</b></label>
            <input type="text" placeholder="Enter Email" name="email" id="email"  value="{{ old('email') }}" required>
            @if($errors->has('email'))
                <p>{{ $errors->first('email') }}</p>
            @endif
            <label for="psw"><b>Password</b></label>
            <input type="password" placeholder="Enter Password" name="psw" required>
            @if($errors->has('psw'))
                <p>{{ $errors->first('psw') }}</p>
            @endif
            <button type="submit">Login</button>
            <label>
                <input type="checkbox" checked="checked" name="remember"> Remember me
            </label>
        </div>

        <div class="container" style="background-color:#f1f1f1">
            <button type="button" class="cancelbtn">Cancel</button>
            <span class="psw">Forgot <a href="reset.html" target="_blank">password</a></span>
        </div>
        <div class="register">
            <a href="{{ asset('/inscription') }}" target="_blank"><button type="button" class="buybtn">Create Account</button></a>
        </div>

    </form>

@endsection
