<?php

namespace App\Http\Controllers;

use App\Models\people;
use Illuminate\Http\Request;

class inscription extends Controller
{
    //
    public function inscription() {

        return view('inscription');
    }
    public function formulaire(Request $request) {

        \request()->validate([

            'email' => ['required', 'email'],
            'psw' => ['required'],
            'password_confirmation' => ['required'],
            ]);

        $people = new people();
        $people->email = $request->email;
        $people->mot_de_passe = bcrypt($request->psw);

        $people->save();

        return "Vous etes bien inscrits !!";
    }



}
