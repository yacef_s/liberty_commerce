<?php

namespace App\Models;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable as BasicAuthenticatable;

class people extends Model implements Authenticatable
{
    use HasFactory;
    use BasicAuthenticatable;

    protected $fillable = ['email', 'mot_de_passe'];

    public function getAuthPassword()
    {
        return $this->mot_de_passe;
    }
    //protected $fillable = ['email', 'mot_de_passe'];

}
