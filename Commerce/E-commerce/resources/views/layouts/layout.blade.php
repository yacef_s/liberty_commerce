<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">


    </head>
    <body>
        <header>
            <div class="my_page">
                <div class="separateur">
                    <div class="logo">
                        <img alt="logo" src="{{asset('image/etna_store.jpg')}}" width="150" height="100"/>
                    </div>
                    <ul>
                        <li class="active"><a class="home" href="{{asset('index.php')}}" target="_blank">Menu</a>

                            <ul class="sous_menu">
                                <li><a href="#">Homme</a></li>
                                <li><a href="#">Femme</a></li>
                                <li><a href="#">Enfant</a></li>
                                <li><a href="#">Accessoires</a></li>
                            </ul>
                        </li>
                        <!--<li><a href="{{ asset('/catalogue') }}" target="_blank">Catalogue</a></li>
                        <li><a href="{{ asset('panier_bis.html') }}" target="_blank">Panier</a></li>-->
                        <li><a href="{{ asset('/connexion') }}" target="_blank">Se Connecter</a></li>
                    </ul>
                </div>
            </div>
        </header>

        @yield('contenu')

        <footer>
            <div class="contenu-footer">
                <div class="reseau">
                    <ul class="liste-media">
                        <li><a href="{{ asset('https://www.facebook.com/') }}" target="_blank"><img src="image/facebook.jpeg" alt="logo_facebook" width="40px">Facebook</a><li>
                        <li><a href="{{ asset('https://twitter.com/') }}" target="_blank"><img src="image/tweeter.jpeg" alt="logo_Tweeter" width="40px">Tweeter</a><li>
                    </ul>
                </div>
            </div>
        </footer>
    </body>
</html>
