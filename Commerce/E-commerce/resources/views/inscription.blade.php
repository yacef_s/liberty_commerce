@extends('layouts/layout')

@section('contenu')
    <link href="{{ asset('css/style_inscription.css') }}" rel="stylesheet">
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <form method="post">
        {{csrf_field()}}
        <div class="container">
            <!-- <h1>Register</h1>-->
            <!-- <p>Please fill in this form to create an account.</p>
             <hr>
             <br><br>-->
            <label for="email"><b>email</b></label>
            <input type="text" placeholder="Enter Email" name="email" id="email"  value="{{ old('email') }}" required>
            <br><br>
            @if($errors->has('email'))
                <p>{{ $errors->first('email') }}</p>
            @endif
            <label for="psw"><b>mot_de_passe</b></label>
            <input type="password" placeholder="Enter Password" name="psw" id="psw" required>
            <br><br>
            @if($errors->has('psw'))
                <p>{{ $errors->first('psw') }}</p>
            @endif
            <label for="psw-repeat"><b>Repeat Password</b></label>
            <input type="password" placeholder="Repeat Password" name="password_confirmation" id="psw-repeat" required>
            <br><br>
            @if($errors->has('password_confirmation'))
                <p>{{ $errors->first('password_confirmation') }}</p>
             @endif

            <!-- <p>By creating an account you agree to our <a href="#">Terms & Privacy</a>.</p>-->
            <button type="submit" class="registerbtn">Register</button>
            <br>
        </div>
        <!--
        <div class="container signin">
          <p>Already have an account? <a href="#">Sign in</a>.</p>
        </div>-->
    </form>

@endsection
