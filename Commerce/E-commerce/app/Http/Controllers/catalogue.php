<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class catalogue extends Controller
{
    public function catalogue() {
        $catalogue = \App\Models\Catalogue::all();
        //dd($catalogue);

        return view('catalogue',compact('catalogue'));
    }
}
