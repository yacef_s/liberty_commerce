@extends('layouts/layout')

@section('contenu')
    <link href="{{ asset('css/style_produit.css') }}" rel="stylesheet">
    {{--dd($produit)--}}
    <div class="sweetcapucherouge">

        <img id="photo" src="{{asset('image/'.$produit->photo)}}" class="img" alt="moi" width="900px" height="900px">
        <h3>{{$produit->nom}}</h3>
        <p class ="titre_description">
            Description
        </p>
        <hr>
        <p class="description">
             {{$produit->description}}
        </p>
        <hr>
        <p class="prix_description">
            {{$produit->prix}}
        </p>
        <div class="dollars">
            <button type="button" class="bouton_achat">direct purchase</button>
        </div>
    </div>

@endsection
