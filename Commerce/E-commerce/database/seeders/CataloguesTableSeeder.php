<?php

namespace Database\Seeders;

use App\Models\Catalogue;
use Illuminate\Database\Seeder;


class CataloguesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $Catalogue= new Catalogue();
        $Catalogue->nom = "sweetcapucheRouge";
        $Catalogue->photo ="sweetcapuche.jpg";
        $Catalogue->prix = 25;
        $Catalogue->description ="Sweat capuche Rouge
        Sweat à capuche 70% coton, 30% polyester
        Regular fit Poche kangourou";
        $Catalogue->save();


        $Catalogue= new Catalogue();
        $Catalogue->nom = "sweetcapucheBleu";
        $Catalogue->photo ="sweetcapucheB.jpg";
        $Catalogue->prix = 30;
        $Catalogue->description = "Sweat capuche Rouge
        Sweat à capuche 70% coton, 30% polyester
        Regular fit Poche kangourou";
        $Catalogue->save();


        $Catalogue= new Catalogue();
        $Catalogue->nom = "sweetcapuchePerso";
        $Catalogue->photo ="sweetcapucheperso.jpg";
        $Catalogue->prix = 35;
        $Catalogue->description = "Sweat capuche Rouge
        Sweat à capuche 70% coton, 30% polyester
        Regular fit Poche kangourou";
        $Catalogue->save();

        $Catalogue= new Catalogue();
        $Catalogue->nom = "sweetcapuchePerso";
        $Catalogue->photo ="sweetcapucherose.jpg";
        $Catalogue->prix = 40;
        $Catalogue->description = "Sweat capuche Rouge
        Sweat à capuche 70% coton, 30% polyester
        Regular fit Poche kangourou";
        $Catalogue->save();

        $Catalogue= new Catalogue();
        $Catalogue->nom = "sweetcapuchePerso";
        $Catalogue->photo ="sweetcapucheMj.jpg";
        $Catalogue->prix = 45;
        $Catalogue->description = "Sweat capuche Rouge
        Sweat à capuche 70% coton, 30% polyester
        Regular fit Poche kangourou";
        $Catalogue->save();


    }
}
